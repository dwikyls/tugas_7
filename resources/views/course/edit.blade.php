@extends('template.index')

@section('container')

<div class="container">
    <div class="row">
        <div class="col">
            <a href="{{ url('/course') }}" class="btn btn-primary"><- Back</a>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-6">
            <form action="/course/update/{{ $course->id }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" name="nama" class="form-control" id="nama" value="{{ $course->nama }}">
                </div>
                <div class="form-group">
                    <label for="sks">SKS</label>
                    <input type="text" name="sks" class="form-control" id="sks" value="{{ $course->sks }}">
                </div>
                <div class="form-group">
                    <label for="kelas">Kelas</label>
                    <input type="text" name="kelas" class="form-control" id="kelas" value="{{ $course->kelas }}">
                </div>
                <button type="submit" class="btn btn-primary">Edit</button>
            </form>
        </div>
    </div>
</div>

@endsection
