@extends('template.index')

@section('container')

<div class="container">
    @if ($message = Session::get('warning'))
        <div class="alert alert-warning alert-block mt-3">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            <strong>{{ $message }}</strong>
        </div>
    @endif
    <div class="row">
        <div class="col">
            <a href="/student/show/{{ $id }}" class="btn btn-primary"><- Kembali</a>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-6">
        @if (!empty($courses[0]))
            <form action="/student/pickCourse" method="POST">
            @csrf
                <table class="table" aria-label="">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Mata Kuliah</th>
                            <th scope="col">Kelas</th>
                            <th scope="col">SKS</th>
                        </tr>
                    </thead>
                    <tbody>
                        <input type="hidden" name="totalSks" value="{{ $totalSks }}">
                        <input type="hidden" name="student_id" value="{{ $id }}">
                        @foreach ($courses as $course)
                        <tr>
                            <th scope="row">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="nama" value="{{ $course->id }}" id="course">
                                </div>
                            </th>
                            <td>{{ $course->nama }}</td>
                            <td>{{ $course->kelas }}</td>
                            <td>{{ $course->sks }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <button type="submit" class="btn btn-primary mt-3">Submit</button>
            </form>
        @else
            <h2>Selamat anda telah menamatkan kampus ini!</h2>
        @endif
        </div>
    </div>
</div>

@endsection
