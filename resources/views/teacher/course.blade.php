@extends('template.index')

@section('container')

<div class="container">
    <div class="row">
        <div class="col">
            <a href="/teacher/show/{{ $id }}" class="btn btn-primary"><- Kembali</a>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-6">
        @if (!empty($courses[0]))
            <form action="/teacher/pickCourse" method="POST">
            @csrf
                <table class="table" aria-label="">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Mata Kuliah</th>
                            <th scope="col">Kelas</th>
                            <th scope="col">SKS</th>
                        </tr>
                    </thead>
                    <tbody>
                        <input type="hidden" name="teacher_id" value="{{ $id }}">
                        @foreach ($courses as $course)
                        <tr>
                            <th scope="row">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="nama" value="{{ $course->id }}" id="course">
                                </div>
                            </th>
                            <td>{{ $course->nama }}</td>
                            <td>{{ $course->kelas }}</td>
                            <td>{{ $course->sks }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <button type="submit" class="btn btn-primary mt-3">Submit</button>
            </form>
        @else
            <h2>Selamat anda telah menamatkan kampus ini!</h2>
        @endif
        </div>
    </div>
</div>

@endsection
