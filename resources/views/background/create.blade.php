@extends('template.index')

@section('container')

<div class="container">
    <div class="row">
        <div class="col">
            <a href="/teacher/show/{{ $id }}" class="btn btn-primary"><- Kembali</a>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-6">
            <form action="/background/store" method="POST">
            @csrf
                <input type="hidden" name="teacher_id" value="{{ $id }}">
                <div class="form-group">
                    <label for="strata">Strata</label>
                    <input type="text" name="strata" class="form-control" id="strata">
                </div>
                <div class="form-group">
                    <label for="jurusan">Jurusan</label>
                    <input type="text" name="jurusan" class="form-control" id="jurusan">
                </div>
                <div class="form-group">
                    <label for="sekolah">Sekolah</label>
                    <input type="text" name="sekolah" class="form-control" id="sekolah">
                </div>
                <div class="form-group">
                    <label for="tahun_mulai">Tahun Mulai</label>
                    <input type="number" name="tahun_mulai" class="form-control" id="tahun_mulai">
                </div>
                <div class="form-group">
                    <label for="tahun_selesai">Tahun Selesai</label>
                    <input type="number" name="tahun_selesai" class="form-control" id="tahun_selesai">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>

@endsection
