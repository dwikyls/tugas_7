<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    public function backgrounds()
    {
        return $this->hasMany('App\Background');
    }

    public function courses()
    {
        return $this->belongsToMany('App\TeacherCourse');
    }
}
