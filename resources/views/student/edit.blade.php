@extends('template.index')

@section('container')

<div class="container">
    <div class="row">
        <div class="col">
            <a href="/student/show/{{ $student->id }}" class="btn btn-primary"><- Back</a>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-6">
            <form action="/student/update/{{ $student->id }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" name="nama" class="form-control" id="nama" value="{{ $student->nama }}">
                </div>
                <div class="form-group">
                    <label for="jenis_kelamin">Jenis Kelamin</label><br>
                    @if($student->jenis_kelamin == 'L')
                    <input type="radio" name="jenis_kelamin" id="L" value="L" checked="checked">
                    <label for="L">Laki-laki</label>
                    <input type="radio" name="jenis_kelamin" id="P" value="P">
                    <label for="P">Perempuan</label>
                    @else
                    <input type="radio" name="jenis_kelamin" id="L" value="L">
                    <label for="L">Laki-laki</label>
                    <input type="radio" name="jenis_kelamin" id="P" value="P" checked="checked">
                    <label for="P">Perempuan</label>
                    @endif
                </div>
                <div class="form-group">
                    <label for="tempat_lahir">Tempat Lahir</label>
                    <input type="text" name="tempat_lahir" class="form-control" id="tempat_lahir" value="{{ $student->tempat_lahir }}">
                </div>
                <div class="form-group">
                    <label for="tanggal_lahir">Tanggal Lahir</label>
                    <input type="text" name="tanggal_lahir" class="form-control" id="tanggal_lahir" value="{{ $student->tanggal_lahir }}">
                </div>
                <button type="submit" class="btn btn-primary">Edit</button>
            </form>
        </div>
    </div>
</div>

@endsection
