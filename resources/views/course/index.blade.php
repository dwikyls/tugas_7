@extends('template.index')

@section('container')

<div class="container">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block mt-3">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            <strong>{{ $message }}</strong>
        </div>
    @endif

    <div class="row">
        <div class="col">
            <a href="{{ url('course/create') }}" class="btn btn-primary">Add course</a>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col">
            <table class="table text-center" aria-label="">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">SKS</th>
                        <th scope="col">Kelas</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($courses as $course)
                    <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{{ $course->nama }}</td>
                        <td>{{ $course->sks }}</td>
                        <td>{{ $course->kelas }}</td>
                        <td>
                            <a href="/course/edit/{{ $course->id }}" class="badge badge-success">Edit</a>
                            <a href="/course/destroy/{{ $course->id }}" class="badge badge-danger">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
