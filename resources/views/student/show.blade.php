@extends('template.index')

@section('container')

<div class="container">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block mt-3">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            <strong>{{ $message }}</strong>
        </div>
    @elseif ($message = Session::get('warning'))
        <div class="alert alert-warning alert-block mt-3">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            <strong>{{ $message }}</strong>
        </div>
    @endif
    <div class="row">
        <div class="col-2 text-left"><a href="{{ url('/student') }}" class="btn btn-primary"><- Kembali</a></div>
        <div class="col-8 text-center"><h1>Detail</h1></div>
        <div class="col-2 text-right"><a href="/student/destroy/{{ $student->id }}" class="btn btn-danger">delete</a></div>
    </div>
    <div class="row mt-3">
        <div class="col-4 text-center d-flex justify-content-center">
            <div class="card" style="width: 18rem;">
                <img src="{{ url('images/default.png') }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Profile</h5>
                    <p class="card-text">Nama Lengkap <br><strong>{{ $student->nama }}</strong></p>
                    <p class="card-text">Jenis Kelamin <br><strong>{{ $student->jenis_kelamin }}</strong></p>
                    <p class="card-text">Tempat, Tanggal Lahir <br><strong>{{ $student->tempat_lahir }}, {{ $student->tanggal_lahir }}</strong></p>
                    <a href="/student/edit/{{ $student->id }}" class="btn btn-secondary">Edit</a>
                </div>
            </div>
        </div>
        <div class="col-8">
            <div class="row">
                <div class="col-10 text-left"><h2>Daftar Mata Kuliah</h2></div>
                <div class="col-2 text-right">
                    <form action="/student/course/{{ $student->id }}" method="GET">
                    @csrf
                        <input type="hidden" name="totalSks" value="{{ $totalSks }}">
                        <button type="submit" class="btn btn-success">Create</button>
                    </form>
                </div>
            </div>
            <table class="table" aria-label="">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">SKS</th>
                        <th scope="col">Kelas</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                @if (!empty($courses[0]))
                    @foreach ($courses as $course)
                    <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{{ $course->nama }}</td>
                        <td>{{ $course->sks }}</td>
                        <td>{{ $course->kelas }}</td>
                        <td>
                            <form action="/student/deleteCourse" method="POST">
                                @csrf
                                <input type="hidden" name="course_id" value="{{ $course->id }}">
                                <input type="hidden" name="student_id" value="{{ $student->id }}">
                                <button class="badge badge-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    <tr>
                        <th scope="row" colspan="2">Total</th>
                        <td colspan="3">{{ $totalSks }}</td>
                    </tr>
                    <tr>
                        <th scope="row" colspan="5">
                            <p>Total SKS dapat diambil <strong class="text-danger">24</strong></p>
                        </th>
                    </tr>
                @else
                    <tr><td><h3>Data Kosong</h3></td></tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
