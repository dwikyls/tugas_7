<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teacher;
use App\Course;
use App\TeacherCourse;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $teachers = Teacher::all();

        return view('teacher.index', compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('teacher.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'gelar' => 'required'
        ]);

        $teacher = new Teacher();

        $teacher->nama = $request->nama;
        $teacher->gelar = $request->gelar;

        $teacher->save();

        return redirect('/teacher')->with(['success' => 'Store success!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $teacher = Teacher::find($id);
        $teacherCourses = TeacherCourse::where('teacher_id', $id)->get();
        
        $courseId = [];

        foreach ($teacherCourses as $teacherCourse) {
            $courseId[] = $teacherCourse->course_id;
        }

        $courses = Course::whereIn('id', $courseId)->get();

        $data = [
            'teacher' => $teacher,
            'backgrounds' => $teacher->backgrounds,
            'courses' => $courses
        ];

        return view('teacher.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher = Teacher::find($id);

        return view('teacher.edit', compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'gelar' => 'required'
        ]);

        $teacher = Teacher::find($id);

        $teacher->nama = $request->nama;
        $teacher->gelar = $request->gelar;

        $teacher->save();

        return redirect('/teacher')->with(['success' => 'Edit success!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Teacher::destroy($id);

        return redirect('/teacher')->with(['success' => 'Delete success!']);
    }

    public function course($id)
    {
        $teacherCourses = TeacherCourse::where('teacher_id', $id)->get();

        $courseId = [];

        foreach ($teacherCourses as $teacherCourse) {
            $courseId[] = $teacherCourse->course_id;
        }

        $courses = Course::whereNotIn('id', $courseId)->get();

        return view('teacher.course', compact('id', 'courses'));
    }

    public function pickCourse(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'teacher_id' => 'required'
        ]);

        $course = Course::find($request->nama);
        
        $teacherCourse =  new TeacherCourse();

        $teacherCourse->teacher_id = $request->teacher_id;
        $teacherCourse->course_id = $course->id;

        $teacherCourse->save();

        return redirect('/teacher/show/'.$request->teacher_id)->with(['success' => 'Berhasil!']);
    }

    public function deleteCourse(Request $request)
    {
        TeacherCourse::where('course_id', $request->course_id)
                    ->where('teacher_id', $request->teacher_id)
                    ->delete();

        return redirect('/teacher/show/'.$request->teacher_id)->with(['success' => 'Berhasil!']);
    }
}
