@extends('template.index')

@section('container')

<div class="container">
    <div class="row">
        <div class="col">
            <a href="/teacher/show/{{ $teacher->id }}" class="btn btn-primary"><- Back</a>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-6">
            <form action="/teacher/update/{{ $teacher->id }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" name="nama" class="form-control" id="nama" value="{{ $teacher->nama }}">
                </div>
                <div class="form-group">
                    <label for="gelar">Gelar</label>
                    <input type="text" name="gelar" class="form-control" id="gelar" value="{{ $teacher->gelar }}">
                </div>
                <button type="submit" class="btn btn-primary">Edit</button>
            </form>
        </div>
    </div>
</div>

@endsection
