@extends('template.index')

@section('container')

<div class="container">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block mt-3">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            <strong>{{ $message }}</strong>
        </div>
    @endif

    <div class="row">
        <div class="col">
            <a href="{{ url('teacher/create') }}" class="btn btn-primary">Add Teacher</a>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col">
            <table class="table text-center" aria-label="">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">NIP</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($teachers as $teacher)
                    <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{{ $teacher->nama }}, {{ $teacher->gelar }}</td>
                        <td>
                            <a href="/teacher/show/{{ $teacher->id }}" class="badge badge-primary">Show</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
