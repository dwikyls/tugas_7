@extends('template.index')

@section('container')

<div class="container">
<div class="row">
        <div class="col">
            <a href="/teacher" class="btn btn-primary"><- Kembali</a>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col">
            <form action="/background/update/{{ $background->id }}" method="POST">
            @csrf
                <div class="form-group">
                    <label for="strata">Strata</label>
                    <input type="text" name="strata" class="form-control" id="strata" value="{{ $background->strata }}">
                </div>
                <div class="form-group">
                    <label for="jurusan">Jurusan</label>
                    <input type="text" name="jurusan" class="form-control" id="jurusan" value="{{ $background->jurusan }}">
                </div>
                <div class="form-group">
                    <label for="sekolah">Sekolah</label>
                    <input type="text" name="sekolah" class="form-control" id="sekolah" value="{{ $background->sekolah }}">
                </div>
                <div class="form-group">
                    <label for="tahun_mulai">Tahun Mulai</label>
                    <input type="text" name="tahun_mulai" class="form-control" id="tahun_mulai" value="{{ $background->tahun_mulai }}">
                </div>
                <div class="form-group">
                    <label for="tahun_selesai">Tahun Selesai</label>
                    <input type="text" name="tahun_selesai" class="form-control" id="tahun_selesai" value="{{ $background->tahun_selesai }}">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>

@endsection
