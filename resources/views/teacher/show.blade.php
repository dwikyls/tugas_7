@extends('template.index')

@section('container')

<div class="container">
    <div class="row">
        <div class="col-2 text-left"><a href="{{ url('/teacher') }}" class="btn btn-primary"><- Kembali</a></div>
        <div class="col-8 text-center"><h1>Detail</h1></div>
        <div class="col-2 text-right"><a href="/teacher/destroy/{{ $teacher->id }}" class="btn btn-danger">delete</a></div>
    </div>
    <div class="row mt-3">
        <div class="col-4 text-center d-flex justify-content-center">
            <div class="card" style="width: 18rem;">
                <img src="{{ url('images/default.png') }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Profile</h5>
                    <p class="card-text">Nama Lengkap <br><strong>{{ $teacher->nama }}, {{ $teacher->gelar }}</strong></p>
                    <a href="/teacher/edit/{{ $teacher->id }}" class="btn btn-secondary">Edit</a>
                </div>
            </div>
        </div>
        <div class="col-8">
            <div class="row">
                <div class="col">
                    <div class="row">
                        <div class="col-10 text-left"><h2>Daftar Mata Kuliah</h2></div>
                        <div class="col-2 text-right"><a href="/teacher/course/{{ $teacher->id }}" class="btn btn-success">Create</a></div>
                    </div>
                    <table class="table" aria-label="">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama</th>
                                <th scope="col">SKS</th>
                                <th scope="col">Kelas</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if (!empty($courses[0]))
                            @foreach ($courses as $course)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td>{{ $course->nama }}</td>
                                <td>{{ $course->sks }}</td>
                                <td>{{ $course->kelas }}</td>
                                <td>
                                    <form action="/teacher/deleteCourse" method="POST">
                                        @csrf
                                        <input type="hidden" name="course_id" value="{{ $course->id }}">
                                        <input type="hidden" name="teacher_id" value="{{ $teacher->id }}">
                                        <button class="badge badge-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        @else
                            <tr><td><h3>Data Kosong</h3></td></tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="row">
                        <div class="col-10"><h2>Daftar Riwayat Pendidikan</h2></div>
                        <div class="col-2 text-right"><a href="/background/create/{{ $teacher->id }}" class="btn btn-success">create</a></div>
                    </div>
                    <table class="table" aria-label="">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Strata</th>
                                <th scope="col">Jurusan</th>
                                <th scope="col">Sekolah</th>
                                <th scope="col">Tahun Mulai</th>
                                <th scope="col">Tahun Lulus</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if (!empty($backgrounds[0]))
                            @foreach ($backgrounds as $background)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td>{{ $background->strata }}</td>
                                <td>{{ $background->jurusan }}</td>
                                <td>{{ $background->sekolah }}</td>
                                <td>{{ $background->tahun_mulai }}</td>
                                <td>{{ $background->tahun_selesai }}</td>
                                <td>
                                    <a href="/background/edit/{{ $background->id }}" class="badge badge-primary">Edit</a>
                                    <a href="/background/delete/{{ $background->id }}" class="badge badge-danger">Delete</a>
                                </td>
                            </tr>
                            @endforeach
                        @else
                            <tr><th scope="row"><h3>Data Kosong</h3></th></tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
