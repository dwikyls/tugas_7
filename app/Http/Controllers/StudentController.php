<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Course;
use App\StudentCourse;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::all();

        return view('student.index', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('student.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'jenis_kelamin' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required'
        ]);

        $student = new Student();

        $student->nama = $request->nama;
        $student->jenis_kelamin = $request->jenis_kelamin;
        $student->tempat_lahir = $request->tempat_lahir;
        $student->tanggal_lahir = $request->tanggal_lahir;

        $student->save();

        return redirect('/')->with(['success' => 'Store success!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::find($id);
        $studentCourses = StudentCourse::where('student_id', $id)->get();
        
        $courseId = [];

        foreach ($studentCourses as $studentCourse) {
            $courseId[] = $studentCourse->course_id;
        }

        $courses = Course::whereIn('id', $courseId)->get();
        
        $totalSks = 0;
        
        foreach ($courses as $course) {
            $totalSks += $course->sks;
        }

        $data = [
            'student' => $student,
            'courses' => $courses,
            'totalSks' => $totalSks
        ];

        return view('student.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::find($id);

        return view('student.edit', compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'jenis_kelamin' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required'
        ]);

        $student = Student::find($id);

        $student->nama = $request->nama;
        $student->jenis_kelamin = $request->jenis_kelamin;
        $student->tempat_lahir = $request->tempat_lahir;
        $student->tanggal_lahir = $request->tanggal_lahir;

        $student->save();

        return redirect('/student/show/'.$id)->with(['success' => 'Edit success!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Student::destroy($id);

        return redirect('/')->with(['success' => 'Delete success!']);
    }

    public function course(Request $request, $id)
    {
        if ($request->totalSks == 24) {
            return back()->with(['warning' => 'Sks tidak boleh lebih dari 24!']);
        }
        
        $totalSks = $request->totalSks;

        $studentCourses = StudentCourse::where('student_id', $id)->get();

        $courseId = [];

        foreach ($studentCourses as $studentCourse) {
            $courseId[] = $studentCourse->course_id;
        }

        $courses = Course::whereNotIn('id', $courseId)->get();

        return view('student.course', compact('id', 'courses', 'totalSks'));
    }

    public function pickCourse(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'student_id' => 'required'
        ]);

        $course = Course::find($request->nama);

        if (($request->totalSks + $course->sks) > 24) {
            return back()->with(['warning' => 'SKS tidak boleh lebih dari 24!']);
        }

        $studentCourse =  new StudentCourse();

        $studentCourse->student_id = $request->student_id;
        $studentCourse->course_id = $course->id;

        $studentCourse->save();

        return redirect('/student/show/'.$request->student_id)->with(['success' => 'Berhasil!']);
    }

    public function deleteCourse(Request $request)
    {
        StudentCourse::where('course_id', $request->course_id)
                    ->where('student_id', $request->student_id)
                    ->delete();

        return redirect('/student/show/'.$request->student_id)->with(['success' => 'Berhasil!']);
    }
}
