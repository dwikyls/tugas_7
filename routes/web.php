<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'StudentController@index');

Route::get('/student', 'StudentController@index');
Route::get('/student/show/{id}', 'StudentController@show');
Route::get('/student/create', 'StudentController@create');
Route::post('/student/store', 'StudentController@store');
Route::get('/student/edit/{id}', 'StudentController@edit');
Route::post('/student/update/{id}', 'StudentController@update');
Route::get('/student/destroy/{id}', 'StudentController@destroy');
Route::get('/student/course/{id}', 'StudentController@course');
Route::post('/student/pickCourse', 'StudentController@pickCourse');
Route::post('/student/deleteCourse', 'StudentController@deleteCourse');

Route::get('/teacher', 'TeacherController@index');
Route::get('/teacher/show/{id}', 'TeacherController@show');
Route::get('/teacher/create', 'TeacherController@create');
Route::post('/teacher/store', 'TeacherController@store');
Route::get('/teacher/edit/{id}', 'TeacherController@edit');
Route::post('/teacher/update/{id}', 'TeacherController@update');
Route::get('/teacher/destroy/{id}', 'TeacherController@destroy');
Route::get('/teacher/course/{id}', 'TeacherController@course');
Route::post('/teacher/pickCourse', 'TeacherController@pickCourse');
Route::post('/teacher/deleteCourse', 'TeacherController@deleteCourse');

Route::get('/course', 'CourseController@index');
Route::get('/course/create', 'CourseController@create');
Route::post('/course/store', 'CourseController@store');
Route::get('/course/edit/{id}', 'CourseController@edit');
Route::post('/course/update/{id}', 'CourseController@update');
Route::get('/course/destroy/{id}', 'CourseController@destroy');

Route::get('/background', 'BackgroundController@index');
Route::get('/background/create/{id}', 'BackgroundController@create');
Route::post('/background/store', 'BackgroundController@store');
Route::get('/background/edit/{id}', 'BackgroundController@edit');
Route::post('/background/update/{id}', 'BackgroundController@update');
Route::get('/background/destroy/{id}', 'BackgroundController@destroy');