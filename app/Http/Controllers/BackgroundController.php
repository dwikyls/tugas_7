<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Background;

class BackgroundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('background.create', compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'teacher_id' => 'required',
            'strata' => 'required',
            'jurusan' => 'required',
            'sekolah' => 'required',
            'tahun_mulai' => 'required',
            'tahun_selesai' => 'required'
        ]);

        $background = new Background();

        $background->teacher_id = $request->teacher_id;
        $background->strata = $request->strata;
        $background->jurusan = $request->jurusan;
        $background->sekolah = $request->sekolah;
        $background->tahun_mulai = $request->tahun_mulai;
        $background->tahun_selesai = $request->tahun_selesai;

        $background->save();

        return redirect('/teacher/show/'.$request->teacher_id)->with(['success' => 'Store riwayat success!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $background = Background::find($id);

        return view('background.edit', compact('background'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'strata' => 'required',
            'jurusan' => 'required',
            'sekolah' => 'required',
            'tahun_mulai' => 'required',
            'tahun_selesai' => 'required'
        ]);

        $background = Background::find($id);

        $background->strata = $request->strata;
        $background->jurusan = $request->jurusan;
        $background->sekolah = $request->sekolah;
        $background->tahun_mulai = $request->tahun_mulai;
        $background->tahun_selesai = $request->tahun_selesai;

        $background->save();

        return redirect('/teacher')->with(['success' => 'Edit riwayat success!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Background::destroy($id);

        return redirect('/teacher')->with(['success' => 'Delete riwayat success!']);
    }
}
